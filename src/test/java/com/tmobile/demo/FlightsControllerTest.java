package com.tmobile.demo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.demo.controllers.FlightsController;
import com.tmobile.demo.models.Flight.Ticket;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(FlightsController.class)
public class FlightsControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void flightShouldReturnASingleJson() throws Exception {
        mockMvc.perform(
                get("/flights/flight")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.Departs").value("2017-04-21"))
                .andExpect(jsonPath("$.Tickets[0].Passenger.FirstName").value("Some name"))
                .andExpect(jsonPath("$.Tickets[0].Passenger.LastName").value("Some other name"))
                .andExpect(jsonPath("$.Tickets[0].Price").value("200"));
    }

    @Test
    public void flightsShouldReturnAnArray() throws Exception {
        mockMvc.perform(
                get("/flights")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].Departs").value("2017-04-21"))
                .andExpect(jsonPath("$[0].Tickets[0].Passenger.FirstName").value("Some name"))
                .andExpect(jsonPath("$[0].Tickets[0].Passenger.LastName").value("Some other name"))
                .andExpect(jsonPath("$[0].Tickets[0].Price").value("200"))
                .andExpect(jsonPath("$[1].Departs").value("2017-04-21"))
                .andExpect(jsonPath("$[1].Tickets[0].Passenger.FirstName").value("Some name"))
                .andExpect(jsonPath("$[1].Tickets[0].Passenger.LastName").isEmpty())
                .andExpect(jsonPath("$[1].Tickets[0].Price").value("400"));
    }

    @Test
    public void flightsTicketTotalShouldReturnTotalW() throws Exception {

        URL payloadUrl = this.getClass().getResource("/tickets.json");
        String payload = new String(
                Files.readAllBytes(
                        Paths.get(
                                payloadUrl.getFile())));


        mockMvc.perform(post("/flights/tickets/total")
                .contentType(MediaType.APPLICATION_JSON)
                .content(payload))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result").value("350"));
    }

    @Test
    public void flightsTicketTotalShouldReturnTotalWithStringLiteral() throws Exception {
        final String payload = "{\n" +
                "  \"Tickets\": [\n" +
                "    {\n" +
                "      \"Passenger\": {\n" +
                "        \"FirstName\": \"Some name\",\n" +
                "        \"LastName\": \"Some other name\"\n" +
                "      },\n" +
                "      \"Price\": 200\n" +
                "    },\n" +
                "    {\n" +
                "      \"Passenger\": {\n" +
                "        \"FirstName\": \"Name B\",\n" +
                "        \"LastName\": \"Name C\"\n" +
                "      },\n" +
                "      \"Price\": 150\n" +
                "    }\n" +
                "  ]\n" +
                "}";


        mockMvc.perform(post("/flights/tickets/total")
                .contentType(MediaType.APPLICATION_JSON)
                .content(payload))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result").value("350"));
    }

    @Test
    public void flightsTicketTotalShouldReturnTotalWithJackson() throws Exception {
        List<Ticket> tickets = new ArrayList<>();
        tickets.add(new Ticket(new Ticket.Passenger("Some name", "Some other name"), 200));
        tickets.add(new Ticket(new Ticket.Passenger("Some name", "Some other name"), 150));

        Map<String, List<Ticket>> ticketsObject = new HashMap<>() {
            {
                put("Tickets", tickets);
            }
        };

        ObjectMapper objMapper = new ObjectMapper();

        String payload = objMapper.writeValueAsString(ticketsObject);

        mockMvc.perform(post("/flights/tickets/total")
                .contentType(MediaType.APPLICATION_JSON)
                .content(payload))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result").value("350"));
    }

    @Test
    public void flightsTicketTotalShouldReturnTotalWithFileFixture() throws Exception {

        URL payloadUrl = this.getClass().getResource("/tickets.json");
        String payload = new String(
                Files.readAllBytes(
                        Paths.get(
                                payloadUrl.getFile())));


        mockMvc.perform(post("/flights/tickets/total")
                .contentType(MediaType.APPLICATION_JSON)
                .content(payload))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result").value("350"));
    }
}
