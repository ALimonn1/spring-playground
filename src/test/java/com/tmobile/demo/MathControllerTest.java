package com.tmobile.demo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.core.StringContains.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest
public class MathControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldReturnPI() throws Exception {
        this.mockMvc.perform(get("/math/pi"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("3.141592653589793")));
    }

    @Test
    public void shouldCalculateDefault() throws Exception{
        this.mockMvc.perform(get("/math/calculate?x=30&y=5"))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().string("35"));

    }
    @Test
    public void shouldCalculateAdd() throws Exception {
        this.mockMvc.perform(get("/math/calculate?operation=add&x=4&y=6"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string("10"));
    }

    @Test
    public void shouldCalculateSubtract() throws Exception {
        this.mockMvc.perform(get("/math/calculate?operation=subtract&x=4&y=6"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string("-2"));

    }

    @Test
    public void shouldCalculateMultiply() throws Exception {
        this.mockMvc.perform(get("/math/calculate?operation=multiply&x=4&y=6"))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().string("24"));
    }

    @Test
    public void shouldReturnVolumeOnAllVerbs() throws Exception{
        this.mockMvc.perform(get("/math/volume/3/4/5"))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().string("The volume of a 3x4x5 rectangle is 60"));

        this.mockMvc.perform(post("/math/volume/7/7/7"))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().string("The volume of a 7x7x7 rectangle is 343"));

        this.mockMvc.perform(delete("/math/volume/3/3/3"))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().string("The volume of a 3x3x3 rectangle is 27"));

        this.mockMvc.perform(put("/math/volume/2/2/2"))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().string("The volume of a 2x2x2 rectangle is 8"));

        this.mockMvc.perform(patch("/math/volume/10/10/10"))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().string("The volume of a 10x10x10 rectangle is 1000"));

    }

    @Test
    public void shouldReturnAreaForAllShapes() throws Exception{
        this.mockMvc.perform(
            post("/math/area")
                .param("type", "circle")
                .param("radius", "4")
        )
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().string("Area of a circle with a radius of 4 is 50.26548245743669"));

        this.mockMvc.perform(
            post("/math/area")
                .param("type", "rectangle")
                .param("width", "4")
                .param("height", "7")
        )
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().string("Area of a 4x7 rectangle is 28.0"));

        this.mockMvc.perform(
            post("/math/area")
                .param("type", "rectangle")
                .param("radius", "4")
        )
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().string("Invalid"));
    }
}
