package com.tmobile.demo.models;

import com.tmobile.demo.interfaces.IShape;

public class RectangleModel implements IShape{
    private int length;
    private int width;
    private int height;

    @Override
    public  String getType(){
        return "rectangle";
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getVolume() {
        return this.height * this.length * this.width;
    }

    @Override
    public double getArea() {
       return this.height* this.width;
    }
    
}
