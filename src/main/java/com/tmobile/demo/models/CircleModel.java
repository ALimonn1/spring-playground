package com.tmobile.demo.models;

import com.tmobile.demo.interfaces.IShape;

public class CircleModel implements IShape{
    private double radius;

    @Override
    public double getArea() {
        return  Math.PI *  Math.pow(radius, 2);
    }

    @Override
    public String getType(){
        return "circle";
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
}