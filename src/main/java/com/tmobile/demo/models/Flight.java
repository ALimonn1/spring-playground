package com.tmobile.demo.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import java.time.LocalDate;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
public class Flight {
    private LocalDate departs;
    private List<Ticket> tickets;

    public LocalDate getDeparts() {
        return departs;
    }

    public void setDeparts(LocalDate departs) {
        this.departs = departs;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }

    @JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
    public static class Ticket {

        public Ticket(Passenger passenger, int price) {
            this.passenger = passenger;
            this.price = price;
        }

        @JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
        public static class Passenger {

            public Passenger(String firstName,  String lastName) {
                this.firstName = firstName;
                this.lastName = lastName;
            }

            public String getFirstName() {
                return firstName;
            }

            public void setFirstName(String firstName) {
                this.firstName = firstName;
            }

            public String getLastName() {
                return lastName;
            }

            public void setLastName(String lastName) {
                this.lastName = lastName;
            }

            private String firstName;
            private String lastName;
        }

        public Passenger getPassenger() {
            return passenger;
        }

        public void setPassenger(Passenger passenger) {
            this.passenger = passenger;
        }

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }

        private Passenger passenger;
        private int price;
    }
}
