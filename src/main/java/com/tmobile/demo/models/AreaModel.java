package com.tmobile.demo.models;

public class AreaModel{
    private String type;
    private int radius;
    private int width;
    private int height;

    public String getArea(){

        if(type.equals("circle")){
            if (radius == 0){
                return "Invalid";
            }
            CircleModel circle = new CircleModel();
            circle.setRadius(radius);
            return "Area of a circle with a radius of " 
            + Integer.toString(radius)
            + " is "
            + Double.toString(circle.getArea());
        }
        if(type.equals("rectangle")){
            if(width == 0 || height == 0){
                return "Invalid";
            }
            RectangleModel rectangle = new RectangleModel();
            rectangle.setWidth(width);
            rectangle.setHeight(height);

            return "Area of a " + Integer.toString(width) 
                + "x"
                + Integer.toString(height)
                + " rectangle is "
                + Double.toString(rectangle.getArea());
        }
        return "Invalid";
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public int getRadius() {
            return radius;
        }

        public void setRadius(int radius) {
            this.radius = radius;
        }

        public int getWidth() {
            return width;
        }

        public void setWidth(int width) {
            this.width = width;
        }

        public int getHeight() {
            return height;
        }

        public void setHeight(int height) {
            this.height = height;
        }
}
