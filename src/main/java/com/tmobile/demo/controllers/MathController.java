package com.tmobile.demo.controllers;

import com.tmobile.demo.models.AreaModel;
import com.tmobile.demo.models.CalculateModel;
import com.tmobile.demo.models.RectangleModel;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/math")
public class MathController {

    @GetMapping("/pi")
    public @ResponseBody String getPI() {
        return "3.141592653589793";
    }

    @GetMapping("/calculate")
    public @ResponseBody String calculate(CalculateModel request) {

        if (isAddition(request)) {
            return addStrings(request);
        }
        if (isSubtract(request)) {
            return subtractStrings(request);
        }
        if (isMultiply(request)) {
            return multiplyStrings(request);
        }
        return "Required parameters not specified";
    }

    @RequestMapping(value = "volume/{length}/{width}/{height}")
    public @ResponseBody String getVolume(RectangleModel rectangle) {
        String prefix = "The volume of a " + rectangle.getLength() + "x" + rectangle.getWidth() + "x"
                + rectangle.getHeight() + " rectangle is ";

        return prefix + Integer.toString(rectangle.getVolume());
    }

    @PostMapping("/area")
    public @ResponseBody String getArea(AreaModel shape) {
        try {
            return shape.getArea();
        } catch (Exception e) {
            return "Invalid";
        }
    }

    private boolean isAddition(CalculateModel request) {
        return request.getOperation() == null || request.getOperation().equals("add");
    }

    private boolean isSubtract(CalculateModel request) {
        return request.getOperation().equals("subtract");
    }

    private boolean isMultiply(CalculateModel request) {
        return request.getOperation().equals("multiply");
    }

    private String addStrings(CalculateModel request) {
        int result = Integer.parseInt(request.getX()) + Integer.parseInt(request.getY());
        return Integer.toString(result);

    }

    private String subtractStrings(CalculateModel request) {
        int result = Integer.parseInt(request.getX()) - Integer.parseInt(request.getY());
        return Integer.toString(result);

    }

    private String multiplyStrings(CalculateModel request) {
        int result = Integer.parseInt(request.getX()) * Integer.parseInt(request.getY());
        return Integer.toString(result);
    }

}
