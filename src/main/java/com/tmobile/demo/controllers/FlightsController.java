package com.tmobile.demo.controllers;

import com.tmobile.demo.models.Flight;
import com.tmobile.demo.models.Flight.Ticket;
import com.tmobile.demo.models.Flight.Ticket.Passenger;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/flights")
public class FlightsController {

    @GetMapping("/flight")
    public Flight getFlight() {
        Flight flightToReturn = new Flight();
        flightToReturn.setDeparts(LocalDate.parse("2017-04-21"));

        List<Ticket> ticketsToSet = new ArrayList<>();

        Ticket.Passenger passengerToAdd = new Passenger("Some name", "Some other name");

        Ticket ticketToAdd = new Ticket(passengerToAdd, 200);
        ticketsToSet.add(ticketToAdd);
        flightToReturn.setTickets(ticketsToSet);
        return flightToReturn;
    }

    @GetMapping
    public List<Flight> getFlights(){
        List<Flight> flightsToReturn = new ArrayList<>();

        flightsToReturn.add(getFlight());
        Flight secondFlight = getFlight();

        secondFlight.getTickets().get(0).getPassenger().setLastName(null);
        secondFlight.getTickets().get(0).setPrice(400);

        flightsToReturn.add(secondFlight);
        return flightsToReturn;
    }

    @PostMapping("/tickets/total")
    public @ResponseBody
    Map calculateTotal(@RequestBody Map<String, Object> inputParam) {
        int total = 0;
        List<Map<String, Object>> requestList = (List<Map<String, Object>>) inputParam.get("Tickets");

        for (Map<String, Object> currTicket : requestList) {
            total += (Integer) currTicket.get("Price");
        }

        int finalTotal = total;
        return new HashMap<String, String>() {
            {
                put("result", Integer.toString(finalTotal));
            }
        };
    }
}
