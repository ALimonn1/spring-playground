package com.tmobile.demo.interfaces;

public interface IShape {
    public double getArea();
    public String getType();
}
